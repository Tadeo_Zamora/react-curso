import {useState} from 'react';

const FormularioHook = () =>{
    //cuando el componente sea montado por primera vez
    //quiero que tenga este estado inicial
    const estadoInicial = {
        nombre : '',
        telefono : '',
        correo : ''
    }
    const [datos,setDatos] = useState(estadoInicial);

    const crearEstado = (objeto) =>{
        setDatos(objeto);
    }
    const enviar = () =>{
        //POST API Rest
        console.log(datos);
    }
    return[
        datos,
        crearEstado,
        enviar
    ]
}

export default FormularioHook;