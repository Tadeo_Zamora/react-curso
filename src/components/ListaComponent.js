import React from 'react';
/*{ propiedades } */
/** las llaves me permiten obtener el valor de la variable y mostrarlo en pantalla */
const ListaComponent = ({titulo,color, seleccionarFruta}) => {
    const mostrarAlerta = (titulo) =>{
        window.alert("Tu fruta es: "+titulo);
    }
    return (
        <li style={{ color : color }} onClick={()=>seleccionarFruta(titulo)} >{titulo}</li>
    );
};

export default ListaComponent;
/**
 * css original margin-top  
 * css react marginTop
 * 
 */