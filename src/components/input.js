import React from 'react';

const InputForm = ({...props}) => {
    const { type, name, placeholder, event } = props;

    const obtenerValores = (evento) =>{
        let name = evento.target.name;
        let value = evento.target.value;
        event({
            name : name,
            value : value
        })
    }

    return (
       <input className="form-control" type={type} name = {name} placeholder={ placeholder } onChange={ obtenerValores } /> 
    );
};

export default InputForm;