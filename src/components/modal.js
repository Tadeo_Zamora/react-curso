import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

const ModalCustom = ({...props}) => {
    //props.mostrar, props.cerrar, props.titulo, props.children
    const { mostrar, cerrar, titulo, children } = props;
    return (
        <>
            <Modal show={mostrar} onHide={cerrar}>
                
                <Modal.Header closeButton>
                    <Modal.Title> {titulo} </Modal.Title>
                </Modal.Header>

                <Modal.Body>{ children }</Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={cerrar}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={cerrar}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default ModalCustom;