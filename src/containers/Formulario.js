//Formulario para el envio de correo
import React from 'react';
import InputForm from '../components/input';
import FormularioHook from '../hooks/FormularioHook'
const Formulario = () =>{
    const [ datos,crearEstado,enviar ] = FormularioHook();
    const obtenerValores = (evento) =>{
        let name = evento.name;
        let value = evento.value;
        crearEstado({ ...datos , [name] : value })
    }
    return(
        <>
            <InputForm 
                type="text" placeholder="Escribe tu nombre" name="nombre"  
                event={ obtenerValores } className="css-personalizado"
            />
            <InputForm 
                type="text" placeholder="Escribe tu telefono" name="telefono"  
                event={ obtenerValores }
            />
            <InputForm 
                type="text" placeholder="Escribe tu correo" name="correo"  
                event={ obtenerValores }
            />
            <button onClick={ enviar }>Enviar</button>
        </>
    )
}
export default Formulario;