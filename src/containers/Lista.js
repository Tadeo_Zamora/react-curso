import React from 'react';
import ListaComponent from '../components/ListaComponent';
import frutas from '../constants/Frutas';
const ListaContainer = ({seleccionarFruta}) => {
    return (
        <ul>
            {
                frutas.map(elemento =>(
                    <ListaComponent key={elemento.fruta} titulo = { elemento.fruta } color={ elemento.color } seleccionarFruta={seleccionarFruta} />
                ))
            }
        </ul>
    );
};

export default ListaContainer;