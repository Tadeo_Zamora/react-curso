import React,{useState} from 'react';
import ListaComponent from './Lista';
import Formulario from './Formulario';
import ModalCustom from '../components/modal';
import ModalHook from '../hooks/ModalHook'
const Home = () =>{
    const [ contador,setContador ] = useState(0)
    const [frutaSeleccionada, setFruta] = useState('');
    const [mostrar, abrir, cerrar] = ModalHook();
    /*
        Objeto 0bj = new Objeto();
    */
    const seleccionarFruta = (valor) =>{
        setFruta(valor);
        abrir()
    }
    return (
        <>
            <h1>Hello Word</h1>
            <ListaComponent seleccionarFruta = { seleccionarFruta } />
            <button onClick={()=>setContador( contador + 1 ) } >Click</button>
            <span>{ contador }</span>
            <hr/>
            { frutaSeleccionada !== '' ? "Esta es la fruta que seleccione: "+frutaSeleccionada : '' }
            <hr/>
            <Formulario />
            <ModalCustom mostrar = { mostrar } cerrar = { cerrar } titulo = "Fruta seleccionada">
                { frutaSeleccionada }
            </ModalCustom>
        </>
    )
}
export default Home;