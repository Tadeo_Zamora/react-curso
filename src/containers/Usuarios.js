import React,{ useEffect } from 'react';
import { GET } from '../services/peticiones'

const Usuarios = () =>{
    useEffect(()=>{
        GET('users')
    },[ ])
    //[ ] indica que solo una vez se ejecuta ese efecto
    // [ variable ] cuando tiene un valor, ese efecto se ejecuta cada vez que el valor de la variable cambia
    return (

        <h2>Usuarios</h2>

    )

}

export default Usuarios;