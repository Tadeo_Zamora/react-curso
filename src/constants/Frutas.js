const frutas = [
    {
        fruta : 'Banana',
        color : 'yellow'
    },
    {
        fruta : "Manzana", 
        color  : "red"
    },
    {
        fruta : "Pera", 
        color : "green"
    }
];
export default frutas;