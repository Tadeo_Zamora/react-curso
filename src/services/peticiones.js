const url = "https://jsonplaceholder.typicode.com/";

const GET = (recurso) => {
    fetch(url+recurso)
    .then(response => response.json())
    .then(json => console.log(json))
}
const POST = (recurso) => {
    fetch(url+recurso)
    .then(response => response.json())
    .then(json => console.log(json))
}

export {
    GET,
    POST
}